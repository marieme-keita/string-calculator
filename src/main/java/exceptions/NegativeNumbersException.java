package exceptions;

public class NegativeNumbersException extends RuntimeException{
    public NegativeNumbersException(String numbers) {
        super("Negatives numbers are not allowed : " + numbers);
    }
}
