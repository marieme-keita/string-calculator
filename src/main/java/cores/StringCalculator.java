package cores;

import exceptions.NegativeNumbersException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringCalculator {
    private final String DEFAULT_DELIMITER = ",";
    private final String LINE_BREAK = "\n";
    private final String CUSTOM_DELIMITER_IDENTIFIER = "//";

    public int add(String numbers) {
        String delimiter = extractDelimiter(numbers.split(LINE_BREAK)[0]);

        if (!delimiter.equals(DEFAULT_DELIMITER)) {
            numbers = numbers.replaceFirst((CUSTOM_DELIMITER_IDENTIFIER + delimiter + LINE_BREAK), "");
        }

        if (numbers.isEmpty()) {
            return 0;
        }

        List<Integer> arrayNumbers = extractNumbers(numbers, delimiter);

        if (hasNegatifNumbers(arrayNumbers)) {
            throw new NegativeNumbersException(extractNegativeNumbers(arrayNumbers)
            );
        }

        return arrayNumbers.stream().reduce(0, Integer::sum);
    }

    private String extractDelimiter(String s) {
        return "[" +
                (s.contains(CUSTOM_DELIMITER_IDENTIFIER) ? s.replace(CUSTOM_DELIMITER_IDENTIFIER, "") : DEFAULT_DELIMITER) +
                LINE_BREAK +
                "]";
    }

    private String extractNegativeNumbers(List<Integer> arrayNumbers) {
        return arrayNumbers
                .stream()
                .filter(integer -> integer < 0)
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }

    private boolean hasNegatifNumbers(List<Integer> arrayNumbers) {
        return arrayNumbers.stream()
                .anyMatch(integer -> integer < 0);
    }

    private List<Integer> extractNumbers(String numbers, String delimiter) {
        return Arrays.stream(numbers.split(delimiter))
                .map(Integer::parseInt)
                .filter(i -> i <= 1000)
                .collect(Collectors.toList());
    }


}
