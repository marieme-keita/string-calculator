import cores.StringCalculator;
import exceptions.NegativeNumbersException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {
    private StringCalculator stringCalculator;

    @BeforeEach
    public void init() {
        stringCalculator = new StringCalculator();
    }

    @Test
    public void should_return_zero_for_an_empty_string() {
        Assertions.assertEquals(0,stringCalculator.add(""));
    }

    @Test
    public void should_return_input_sum() {
        Assertions.assertEquals(1,stringCalculator.add("1"));
        Assertions.assertEquals(1,stringCalculator.add("0,1"));
        Assertions.assertEquals(3,stringCalculator.add("1,2"));
        Assertions.assertEquals(15,stringCalculator.add("10,5"));
        Assertions.assertEquals(34,stringCalculator.add("11,23"));
        Assertions.assertEquals(1001,stringCalculator.add("1,1000"));

        Assertions.assertEquals(6,stringCalculator.add("1,2,3"));
    }

    @Test
    public void should_return_zero_when_string_is_empty() {
        String numbers = "";
        int result = stringCalculator.add(numbers);
        Assertions.assertEquals(0,result);
    }

    @Test
    public void should_handle_new_lines_between_numbers() {
        Assertions.assertEquals(6,stringCalculator.add("1\n3,2"));
        Assertions.assertEquals(6,stringCalculator.add("1\n2,3"));
    }

    @Test
    public void should_support_different_delimiter() {
        Assertions.assertEquals(3,stringCalculator.add("//;\n1;2"));
    }


    @Test
    public void should_throw_an_exception_when_negative_number_is_called() {
        NegativeNumbersException exception = Assertions.assertThrows(NegativeNumbersException.class,() -> {stringCalculator.add("-5");});
        Assertions.assertTrue(exception.getMessage().contains("negatives numbers are not allowed"));
    }


   @Test
    public void should_ignore_numbers_bigger_than_1000() {
        Assertions.assertEquals(2,stringCalculator.add("2,1002"));
    }
}
